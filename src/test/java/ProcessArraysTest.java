import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Bill Sullivan on 15/03/15.
 */
public class ProcessArraysTest {

    private ProcessArrays processArrays = new ProcessArrays();

    @Test
    public void findArray_Success_FirstMatch() {
        int[]  first = new int[]{2,3,4,5};
        int[] second = new int[]{4,5};
        int foundAtPosition = processArrays.findInArray(first, second);
        //displayResult(foundAtPosition);
        Assert.assertEquals(foundAtPosition, 2);
    }

    @Test
    public void findArray_Success_SecondMatch() {
        int[]  first = new int[]{2,3,4,1,2,3,4,5};
        int[] second = new int[]{4,5};
        int foundAtPosition = processArrays.findInArray(first, second);
        //displayResult(foundAtPosition);
        Assert.assertEquals(foundAtPosition, 6);
    }

    @Test
    public void findArray_Success_FourthMatch() {
        int[]  first = new int[]{2,3,4,1,2,3,4,7,6,5,4,1,0,1,2,3,4,5,12,11,18};
        int[] second = new int[]{4,5};
        int foundAtPosition = processArrays.findInArray(first, second);
        //displayResult(foundAtPosition);
        Assert.assertEquals(foundAtPosition, 16);
    }

    // dont really need to call this, instead use junit's Assert.assertEquals() method.
    private void displayResult(int foundAtPosition) {
        if (foundAtPosition<=-1){
            System.out.println("unable to find second array in first.");
        }else{
            System.out.println("found second array at position = "+foundAtPosition);
        }
    }
    // this is a change. testing git.
    // another test.
    // THREE
    // FOUR
    // FIFTH
    // SIXTH
    // SEVENTH
    // EIGTH
    // NINTH
}
