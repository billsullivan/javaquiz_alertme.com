/**
 * Created by Bill Sullivan on 15/03/15.
 */
public class ProcessedStatus {

    private boolean successful;
    private int position;

    public boolean isSuccessful() {
        return successful;
    }

    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
