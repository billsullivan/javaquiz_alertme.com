/**
 * Created by Bill Sullivan on 15/03/15.
 */
public class ProcessArrays {

    private ProcessedStatus processArrays(int[] first, int[] second, int index) {
        boolean openToken = false;
        boolean closeToken = false;
        int openPosition = -1;
        int lengthFoundTokens = 0;
        //for (int a=0; a<first.length; a++) {
        for (int a=index; a<first.length; a++) {
            if (openToken==false){
                // looking for openToken.
                if (first[a] == second[0]) {
                    openToken=true;
                    openPosition=a;
                    lengthFoundTokens++;
                    for (int b=1; b<second.length; b++) {
                        if (first[(a+b)]==second[b]) {
                            lengthFoundTokens++;
                        } else {
                            closeToken=true;
                        }
                    }
                    //System.out.println("a="+a);
                    break;
                }
            }
        }

        ProcessedStatus processedStatus = new ProcessedStatus();
        // does lengthFoundTokens == second.length ?
        if (lengthFoundTokens == second.length) {
            processedStatus.setSuccessful(true);
            processedStatus.setPosition(openPosition);
        }else{
            processedStatus.setPosition(openPosition+1);
            processedStatus.setSuccessful(false);
        }
        return processedStatus;
    }

    public int findInArray(int[] first, int[] second) {
        //ProcessArrays processArrays = new ProcessArrays();
        int recordFoundPosition = -1; // not a valid position. i.e. not found it.
        boolean isSuccessful = false;
        int uptoPosition = 0;
        do {
            ProcessedStatus processedStatus = processArrays(first, second, uptoPosition /*index*/);
            isSuccessful = processedStatus.isSuccessful();
            if (!isSuccessful) {
                uptoPosition = processedStatus.getPosition();
            } else {
                recordFoundPosition = processedStatus.getPosition();
                break;
            }
        } while ( (!isSuccessful)&&(uptoPosition<first.length) );
        return recordFoundPosition;
    }
    // this is a change. testing git.
    // another test.
    // THREE
    // FOUR
    // FIFTH
    // SIXTH
    // SEVENTH
    // EIGTH
    // NINTH
}
